package oax_testlab

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "OK")
	})

	err := http.ListenAndServe(":" + port, nil)

	if err != nil{
		log.Fatalf("Error during running lab")
	}
}
